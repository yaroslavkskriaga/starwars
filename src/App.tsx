import React from 'react';
import { ThemeProvider } from '@mui/material';
import { theme } from './Shared/Theme';
import { LayoutPage } from './Shared/Layout/LayoutPage';
import { DashboardPage } from './Modules/Dashboard/DashboardPage';

function App() {
  return (
    <ThemeProvider theme={theme}>
      <LayoutPage centred>
        <DashboardPage />
      </LayoutPage>
    </ThemeProvider>
  );
}

export default App;
