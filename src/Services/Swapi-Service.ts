import { AxiosResponse } from 'axios';
import { CharactersInterface, FilmsInterface } from '../Api/Interfaces';
import { API_LIST_FILMS, API_LIST_PEOPLE } from '../Api/Api';
import { httpGet } from './HTTP-Service';

const getPeople = (): Promise<CharactersInterface[]> => httpGet(API_LIST_PEOPLE)
  .then((data: AxiosResponse<any>) => data.data.results);

const getFilms = (): Promise<FilmsInterface[]> => httpGet(API_LIST_FILMS)
  .then((data: AxiosResponse<any>) => data.data.results);

const SwapiService = {
  getPeople,
  getFilms,
};

export default SwapiService;
