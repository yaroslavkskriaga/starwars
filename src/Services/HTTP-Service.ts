import axios, { AxiosResponse } from 'axios';
import { API_BASE } from '../Shared/config';

export async function httpGet(url: string): Promise<AxiosResponse<unknown>> {
  const request = axios.create({
    baseURL: API_BASE,
    headers: {
      'Content-type': 'application/json',
    },
  }).get((url));

  request.catch((error) => {
    let errorMessage = '[HTTP-Service Internal Error]';
    if (error instanceof Error) {
      errorMessage = error.message;
    }
    console.error(errorMessage);
  });

  return request;
}
