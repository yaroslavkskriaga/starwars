import React, { ReactElement } from 'react';
import {
  Box,
  Button, Card, CardActions, CardContent, CircularProgress, IconButton,
} from '@mui/material';
import avatar from './Assets/avatar.png';
import { FilmsInterface } from '../../Api/Interfaces';

interface CharacterCardInterface {
  name: string;
  films: FilmsInterface[] | null;
  remove(e: React.MouseEvent<HTMLButtonElement>): void;
  openCharacterDialog: () => void;
}

export function CharacterCard({
  name, films, remove, openCharacterDialog,
}: CharacterCardInterface): ReactElement {
  return (
    <Card>
      <CardContent>
        <IconButton style={{ float: 'right' }} name={name} onClick={(event:React.MouseEvent<HTMLButtonElement>) => remove(event)}>X</IconButton>
        <Box display="flex" alignItems="center" flexWrap="wrap">
          <img width="250px" height="250px" style={{ borderRadius: '20%' }} alt="icon" src={avatar} />
          <Box ml={4}>
            <h2>
              {name}
            </h2>
            <Box height="20px" />
            <h4>Filmography</h4>
            {films ? films.map((item:FilmsInterface) => <ul key={item.title}><li>{item.title}</li></ul>) : <CircularProgress />}
          </Box>
        </Box>
      </CardContent>
      <CardActions>
        <Button onClick={openCharacterDialog} variant="contained">Learn More</Button>
      </CardActions>
    </Card>
  );
}
