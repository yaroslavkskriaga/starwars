import React, { ReactElement, useCallback } from 'react';
import {
  Box, Fade, OutlinedInput, IconButton, Modal, Typography, Button, Input,
} from '@mui/material';
import { CharactersInterface } from '../../Api/Interfaces';
import { CharacterModalStyles } from './Styles/CharacterModalStyles';

interface CharacterModalInterface {
  close: () => void;
  open: boolean;
  characterModalData: CharactersInterface | null;
}

export function CharacterModal({ close, open, characterModalData }:CharacterModalInterface): ReactElement {
  const handleCloseCharacterModal = useCallback((): void => {
    close();
  }, [close]);

  return (
    <Modal open={open} onClose={close}>
      <Fade in={open}>
        <Box sx={CharacterModalStyles}>
          <Box display="flex" alignItems="center" justifyContent="space-between" flexDirection="row-reverse">
            <IconButton onClick={handleCloseCharacterModal}>X</IconButton>
            <Typography component="h2">
              Edit character
            </Typography>
          </Box>
          <Box height="20px" />
          <div style={{ color: 'white', marginBottom: '5px' }}>Name</div>
          <OutlinedInput
            fullWidth
            placeholder="Name"
            id="name"
            type="text"
            defaultValue={characterModalData?.name}
          />
          <Box height="10px" />
          <div style={{ color: 'white', marginBottom: '5px' }}>Mass</div>
          <OutlinedInput
            fullWidth
            id="mass"
            placeholder="Mass"
            type="number"
            defaultValue={characterModalData?.mass}
          />
          <Box height="10px" />
          <div style={{ color: 'white', marginBottom: '5px' }}>Height</div>
          <OutlinedInput
            fullWidth
            id="height"
            placeholder="Height"
            type="number"
            defaultValue={characterModalData?.height}
          />
          <Box height="10px" />
          <div style={{ color: 'white', marginBottom: '5px' }}>Gender</div>
          <OutlinedInput
            fullWidth
            id="gender"
            placeholder="Gender"
            type="text"
            defaultValue={characterModalData?.gender}
          />
          <Box height="10px" />
          <div style={{ color: 'white', marginBottom: '5px' }}>Upload Photo</div>
          <Box display="flex">
            <Input id="contained-button-file" type="file" />
            <Button>Upload</Button>
            <Button>Remove</Button>
          </Box>
          <Box height="30px" />
          <Box display="flex" justifyContent="space-between">
            <Button variant="contained">Save</Button>
            <Button variant="contained">Delete</Button>
          </Box>
        </Box>
      </Fade>
    </Modal>
  );
}
