import { makeStyles } from '@mui/styles';
import { Theme } from '@mui/material';

export const useStylesLayout = makeStyles((theme: Theme) => ({
  container: {
    width: '100%',
    placeSelf: 'center',
    maxWidth: theme.breakpoints.values.lg,
  },
}));
