import React, { ReactElement, useEffect, useState } from 'react';
import { LinearProgress } from '@mui/material';
import { Dashboard } from './Dashboard';
import { CharactersInterface, FilmsInterface } from '../../Api/Interfaces';
import SwapiService from '../../Services/Swapi-Service';

export function DashboardPage(): ReactElement {
  const [charactersList, setCharactersList] = useState<CharactersInterface[]>();
  const [filmsList, setFilmsList] = useState<FilmsInterface[]>();

  function fetchCharactersList() {
    return SwapiService
      .getPeople()
      .then((data: CharactersInterface[]) => {
        setCharactersList(data);
      });
  }

  function fetchFilmsList() {
    return SwapiService
      .getFilms()
      .then((data: FilmsInterface[]) => {
        setFilmsList(data);
      });
  }

  useEffect(() => {
    fetchCharactersList();
    fetchFilmsList();
  }, []);

  if (!charactersList && !filmsList) return <LinearProgress />;

  return (
    <Dashboard charactersList={charactersList} filmsList={filmsList} />
  );
}
