import { FilmsInterface } from '../../../Api/Interfaces';

export function selectFilms(characterFilms: string[], films: FilmsInterface[]):FilmsInterface[] | null {
  if (films.length > 0) {
    return films.filter((item: FilmsInterface) => characterFilms.includes(item.url));
  }
  return null;
}
