import React, {
  ReactElement, useCallback, useMemo, useState,
} from 'react';
import { Box } from '@mui/material';
import { CharacterCard } from '../../Components/CharacterCard/CharacterCard';
import { CharactersInterface, FilmsInterface } from '../../Api/Interfaces';
import { selectFilms } from './Utils/DashboardSelectors';
import { SearchBar } from '../../Components/SearchBar/SearchBar';
import { CharacterModal } from '../../Components/CharacterModal/CharacterModal';

interface DashboardInterface {
  charactersList: CharactersInterface[] | undefined;
  filmsList: FilmsInterface[] | undefined;

}

export function Dashboard({ charactersList, filmsList }: DashboardInterface): ReactElement {
  const [filter, setFilter] = useState('');
  const [characters, setCharacters] = useState<CharactersInterface[] | undefined>(charactersList);
  const [characterModalState, setCharacterModalState] = useState(false);
  const [characterModalData, setCharacterModalData] = useState<CharactersInterface | null>(null);

  const filteredData = useMemo((): CharactersInterface[] | undefined => {
    if (filter === '') return characters;
    return characters?.filter(
      (item) => item.name.toLowerCase().includes(filter),
    );
  }, [characters, filter]);

  const handleRemoveItem = useCallback((event: React.MouseEvent<HTMLButtonElement>) => {
    const name = (event.target as HTMLButtonElement).getAttribute('name');
    setCharacters(filteredData?.filter((item) => item.name !== name));
  }, [charactersList, filteredData]);

  const handleCharacterModal = useCallback((item: CharactersInterface) => {
    setCharacterModalState(true);
    setCharacterModalData(item);
  }, []);

  return (
    <Box>
      <CharacterModal characterModalData={characterModalData} close={() => setCharacterModalState(false)} open={characterModalState} />
      <SearchBar onSearch={(search: string) => setFilter(search)} />
      <Box height="20px" />
      {filteredData && filteredData.length > 0 ? filteredData?.map((item: CharactersInterface) => (
        <Box key={item.name}>
          <CharacterCard
            openCharacterDialog={() => handleCharacterModal(item)}
            remove={handleRemoveItem}
            films={selectFilms(item.films, filmsList || [])}
            name={item.name}
          />
          <Box height="20px" />
        </Box>
      )) : <h1 style={{ color: 'white' }}>No data left</h1>}
    </Box>
  );
}
